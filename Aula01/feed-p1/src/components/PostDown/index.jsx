import styles from '../Post/index.module.css';
import { Comment } from '../Comment'

export function PostDown() {
  return (
    <aside className={styles.background}>

      <div className={styles.container}>

        <div className={styles.group}>

          <div className={styles.profile}>
            <img className={styles.avatar} src="https://gitlab.com/uploads/-/system/user/avatar/11143896/avatar.png?width=96" />

            <div className={styles.description}>
              <strong className={styles.name}>Mariana Rocha</strong>
              <span>Analista de Inovação | Marketing | Crie_TI Univates | Futura Dev</span>
            </div>
          </div>

          <div>
            <time>Publicado há 10h</time>
          </div>

        </div>

        <p><br />
          Oi, pessoal!<br />
          <br />
          Já viram o meu novo projeto no figma? Não deixem de visitar minha página!<br />
          <br />
          <a>https://figma.com/</a><br />
          <br /></p>
        <a src='#' className={styles.hash}>
          #novoprojeto #figma #ui/ux
          <br />
          <br />
        </a>

        <div className={styles.line}></div>

        <div className={styles.comment}>
          <strong>Deixe seu feedback</strong>
          <textarea type="text" placeholder='Escreva aqui' className={styles.input} />
          <button className={styles.button}>
            Publicar
          </button>
        </div>

      <Comment />

      </div>
    </aside>
  )
}