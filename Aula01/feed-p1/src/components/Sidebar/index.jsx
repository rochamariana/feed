import styles from './index.module.css';

export function Sidebar() {
  return (
    <aside className={styles.sidebar}>
      <img className={styles.cover} src="https://images.unsplash.com/photo-1530303263041-b5ca33678f04?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80" alt="Imagem de capa" />

      <div className={styles.profile}>
        <img className={styles.avatar} src="https://gitlab.com/uploads/-/system/user/avatar/11143896/avatar.png?width=96" />
        
        <div className={styles.description}>
          <strong className={styles.name}>Mariana Rocha</strong>
          <span>Analista de Inovação | Marketing | Crie_TI Univates | Futura Dev</span>
        </div>

      </div>

    </aside>
  )
}