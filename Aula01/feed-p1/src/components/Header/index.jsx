import styles from './index.module.css';
import crieLogo from '../../assets/logo-crie-ti 1.png' /* Importamos o logo para a variável crieLogo */

export function Header() {
  return (
    <header className={styles.header} >
      <img src={crieLogo} alt="Logo do Crie TI"></img>
    </header>
  )
}