import styles from './index.module.css';
import style from '../Post/index.module.css'

export function Comment() {
  return (
    <div>
      <div className={styles.bloco}>
        <div className={styles.container}>

          <img className={styles.avatar} src="https://images.unsplash.com/photo-1564564321837-a57b7070ac4f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=876&q=80" />

          <div className={styles.comment}>

            <div className={styles.user}>
              <strong className={styles.name}>Wade Warren</strong>
              <time>Comentado há 2h</time>
              <span>Adorei seu novo trabalho, Mari! o/</span>
              <br />
              <br />
            </div>
            <div className={styles.delete}>
              <i class="fa-regular fa-trash-can"></i>
            </div>

          </div>

        </div>
      </div>

      {/* MUDA COMENTÁRIO */}

      <div className={styles.bloco}>

        <div className={styles.containerLike}>
          <div className={styles.like}>
            <i class="fa-regular fa-thumbs-up"></i>
          </div>
          <strong>03</strong>
        </div>

        <div>
          <div className={styles.container}>

            <img className={styles.avatar} src="https://images.unsplash.com/photo-1508214751196-bcfd4ca60f91?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80" />

            <div className={styles.comment}>

              <div className={styles.user}>
                <strong className={styles.name}>Darrell Steward</strong>
                <time>Comentado há 2h</time>
                <span>Arrasou! 👏👏</span>
                <br />
                <br />
              </div>
              <div className={styles.delete}>
                <i class="fa-regular fa-trash-can"></i>
              </div>
            </div>

          </div>
        </div>

        <div className={styles.containerLike}>
          <div className={styles.like}>
            <i class="fa-regular fa-thumbs-up"></i>
          </div>
          <strong>33</strong>
        </div>
      </div>

      {/* MUDA COMENTÁRIO */}

      <div className={styles.bloco}>
        <div className={styles.container}>

          <img className={styles.avatar} src="https://images.unsplash.com/photo-1525875975471-999f65706a10?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjd8fHBlcnNvbnxlbnwwfDB8MHx8&auto=format&fit=crop&w=500&q=60" />

          <div className={styles.comment}>

            <div className={styles.user}>
              <strong className={styles.name}>Courtney Henry</strong>
              <time>Comentado há 6h</time>
              <span>Incrível, Mari! Parabéns!</span>
              <br />
              <br />
            </div>
            <div className={styles.delete}>
              <i class="fa-regular fa-trash-can"></i>
            </div>

          </div>

        </div>

        <div className={styles.containerLike}>
          <div className={styles.like}>
            <i class="fa-regular fa-thumbs-up"></i>
          </div>
          <strong>10</strong>
        </div>

      </div>
    </div>
  )
}