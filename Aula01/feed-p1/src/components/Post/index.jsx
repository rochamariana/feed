import styles from './index.module.css';

export function Post() {
  return (
    <aside className={styles.background}>

      <div className={styles.container}>

        <div className={styles.group}>

          <div className={styles.profile}>
            <img className={styles.avatar} src="https://gitlab.com/uploads/-/system/user/avatar/11143896/avatar.png?width=96" />

            <div className={styles.description}>
              <strong className={styles.name}>Mariana Rocha</strong>
              <span>Analista de Inovação | Marketing | Crie_TI Univates | Futura Dev</span>
            </div>
          </div>

          <div>
            <time>Publicado há 1h</time>
          </div>

        </div>

        <p><br />
          Oi, rede!<br />
          <br />
          Acabei de publicar novos projetos no meu gitlab, segue o link para darem uma conferida!<br />
          <br />
          <a>https://gitlab.com/rochamariana</a><br />
          <br /></p>
        <a src='#' className={styles.hash}>
          #novoprojeto #git #frontend
          <br />
          <br />
        </a>

        <div className={styles.line}></div>

        <div className={styles.comment}>
          <strong>Deixe seu feedback</strong>
          <textarea type="text" placeholder='Escreva aqui' className={styles.input} />
          <button className={styles.button}>
            Publicar
          </button>
        </div>

      </div>
    </aside>
  )
}